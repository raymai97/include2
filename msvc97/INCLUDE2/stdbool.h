#ifndef STDBOOL_H
#define STDBOOL_H

#undef bool
#define bool  int
#undef true
#define true  (1==1)
#undef false
#define false (1==0)

#endif//STDBOOL_H
